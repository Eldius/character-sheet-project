package net.eldiosantos.rpg.sheet.repository.hibernate.skill;

import net.eldiosantos.rpg.sheet.model.skill.KnowledgeSkill;
import net.eldiosantos.rpg.sheet.repository.hibernate.base.BaseRepository;
import net.eldiosantos.rpg.sheet.repository.skill.KnowledgeSkillRepository;

/**
 * Created by Eldius on 13/05/2015.
 */
public class KnowledgeSkillRepositoryHibernateImpl extends BaseRepository<KnowledgeSkill, Long> implements KnowledgeSkillRepository {
}
