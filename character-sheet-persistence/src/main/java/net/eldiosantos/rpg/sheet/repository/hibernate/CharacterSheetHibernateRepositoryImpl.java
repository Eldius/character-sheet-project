package net.eldiosantos.rpg.sheet.repository.hibernate;

import net.eldiosantos.rpg.sheet.model.CharacterSheet;
import net.eldiosantos.rpg.sheet.repository.CharacterSheetRepository;
import net.eldiosantos.rpg.sheet.repository.hibernate.base.BaseRepository;

/**
 * Created by Eldius on 12/05/2015.
 */
public class CharacterSheetHibernateRepositoryImpl extends BaseRepository<CharacterSheet, Long> implements CharacterSheetRepository {
}
