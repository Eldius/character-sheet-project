package net.eldiosantos.rpg.sheet.repository.hibernate.character.skill;

import net.eldiosantos.rpg.sheet.model.character.skill.CharacterExpertiseSkill;
import net.eldiosantos.rpg.sheet.repository.character.skill.CharacterExpertiseSkillRepository;
import net.eldiosantos.rpg.sheet.repository.hibernate.base.BaseRepository;

/**
 * Created by Eldius on 13/05/2015.
 */
public class CharacterExpertiseSkillHibernateImpl extends BaseRepository<CharacterExpertiseSkill, Long> implements CharacterExpertiseSkillRepository {
}
