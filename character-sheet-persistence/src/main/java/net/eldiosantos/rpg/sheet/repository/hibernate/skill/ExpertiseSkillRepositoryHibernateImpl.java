package net.eldiosantos.rpg.sheet.repository.hibernate.skill;

import net.eldiosantos.rpg.sheet.model.skill.ExpertiseSkill;
import net.eldiosantos.rpg.sheet.repository.hibernate.base.BaseRepository;
import net.eldiosantos.rpg.sheet.repository.skill.ExpertiseSkillRepository;

/**
 * Created by Eldius on 13/05/2015.
 */
public class ExpertiseSkillRepositoryHibernateImpl extends BaseRepository<ExpertiseSkill, Long> implements ExpertiseSkillRepository {
}
