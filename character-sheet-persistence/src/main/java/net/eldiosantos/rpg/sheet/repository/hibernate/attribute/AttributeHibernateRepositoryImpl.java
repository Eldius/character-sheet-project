package net.eldiosantos.rpg.sheet.repository.hibernate.attribute;

import net.eldiosantos.rpg.sheet.model.attribute.Attribute;
import net.eldiosantos.rpg.sheet.repository.attribute.AttributeRepository;
import net.eldiosantos.rpg.sheet.repository.hibernate.base.BaseRepository;

/**
 * Created by Eldius on 12/05/2015.
 */
public class AttributeHibernateRepositoryImpl extends BaseRepository<Attribute, Long> implements AttributeRepository {
}
