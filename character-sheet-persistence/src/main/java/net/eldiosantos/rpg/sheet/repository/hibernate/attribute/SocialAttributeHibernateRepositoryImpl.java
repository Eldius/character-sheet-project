package net.eldiosantos.rpg.sheet.repository.hibernate.attribute;

import net.eldiosantos.rpg.sheet.model.attribute.SocialAttribute;
import net.eldiosantos.rpg.sheet.repository.attribute.SocialAttributeRepository;
import net.eldiosantos.rpg.sheet.repository.hibernate.base.BaseRepository;

/**
 * Created by Eldius on 12/05/2015.
 */
public class SocialAttributeHibernateRepositoryImpl extends BaseRepository<SocialAttribute, Long> implements SocialAttributeRepository {
}
