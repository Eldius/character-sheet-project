package net.eldiosantos.rpg.sheet.repository.hibernate.attribute;

import net.eldiosantos.rpg.sheet.model.attribute.MentalAttribute;
import net.eldiosantos.rpg.sheet.repository.attribute.MentalAttributeRepository;
import net.eldiosantos.rpg.sheet.repository.hibernate.base.BaseRepository;

/**
 * Created by Eldius on 12/05/2015.
 */
public class MentalAttributeHibernateRepositoryImpl extends BaseRepository<MentalAttribute, Long> implements MentalAttributeRepository {
}
