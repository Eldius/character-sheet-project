package net.eldiosantos.rpg.sheet.repository.hibernate.skill;

import net.eldiosantos.rpg.sheet.model.skill.TalentSkill;
import net.eldiosantos.rpg.sheet.repository.hibernate.base.BaseRepository;
import net.eldiosantos.rpg.sheet.repository.skill.TalentSkillRepository;

/**
 * Created by Eldius on 13/05/2015.
 */
public class TalentSkillRepositoryHibernateImpl extends BaseRepository<TalentSkill, Long> implements TalentSkillRepository {
}
