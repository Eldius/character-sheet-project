package net.eldiosantos.rpg.sheet.repository.hibernate.character.attribute;

import net.eldiosantos.rpg.sheet.model.character.attribute.CharacterSocialAttribute;
import net.eldiosantos.rpg.sheet.repository.character.attribute.CharacterSocialAttributeRepository;
import net.eldiosantos.rpg.sheet.repository.hibernate.base.BaseRepository;

/**
 * Created by Eldius on 12/05/2015.
 */
public class CharacterSocialAttributeHibernateRepositoryImpl extends BaseRepository<CharacterSocialAttribute, Long> implements CharacterSocialAttributeRepository {
}
