package net.eldiosantos.rpg.sheet.repository.hibernate.character.skill;

import net.eldiosantos.rpg.sheet.model.character.skill.CharacterSkill;
import net.eldiosantos.rpg.sheet.repository.character.skill.CharacterSkillRepository;
import net.eldiosantos.rpg.sheet.repository.hibernate.base.BaseRepository;

/**
 * Created by Eldius on 13/05/2015.
 */
public class CharacterSkillHibernateImpl extends BaseRepository<CharacterSkill, Long> implements CharacterSkillRepository {
}
