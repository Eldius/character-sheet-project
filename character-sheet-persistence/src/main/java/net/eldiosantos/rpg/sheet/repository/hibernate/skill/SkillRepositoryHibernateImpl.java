package net.eldiosantos.rpg.sheet.repository.hibernate.skill;

import net.eldiosantos.rpg.sheet.model.skill.Skill;
import net.eldiosantos.rpg.sheet.repository.hibernate.base.BaseRepository;
import net.eldiosantos.rpg.sheet.repository.skill.SkillRepository;

/**
 * Created by Eldius on 13/05/2015.
 */
public class SkillRepositoryHibernateImpl extends BaseRepository<Skill, Long> implements SkillRepository {
}
