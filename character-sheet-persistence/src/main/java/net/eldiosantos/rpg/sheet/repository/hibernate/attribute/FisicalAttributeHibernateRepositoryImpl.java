package net.eldiosantos.rpg.sheet.repository.hibernate.attribute;

import net.eldiosantos.rpg.sheet.model.attribute.FisicalAttribute;
import net.eldiosantos.rpg.sheet.repository.attribute.FisicalAttributeRepository;
import net.eldiosantos.rpg.sheet.repository.hibernate.base.BaseRepository;

/**
 * Created by Eldius on 12/05/2015.
 */
public class FisicalAttributeHibernateRepositoryImpl extends BaseRepository<FisicalAttribute, Long> implements FisicalAttributeRepository {
}
