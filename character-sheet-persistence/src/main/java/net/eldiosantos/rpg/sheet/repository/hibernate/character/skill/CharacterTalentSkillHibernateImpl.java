package net.eldiosantos.rpg.sheet.repository.hibernate.character.skill;

import net.eldiosantos.rpg.sheet.model.character.skill.CharacterTalentSkill;
import net.eldiosantos.rpg.sheet.repository.character.skill.CharacterTalentSkillRepository;
import net.eldiosantos.rpg.sheet.repository.hibernate.base.BaseRepository;

/**
 * Created by Eldius on 13/05/2015.
 */
public class CharacterTalentSkillHibernateImpl extends BaseRepository<CharacterTalentSkill, Long> implements CharacterTalentSkillRepository {
}
