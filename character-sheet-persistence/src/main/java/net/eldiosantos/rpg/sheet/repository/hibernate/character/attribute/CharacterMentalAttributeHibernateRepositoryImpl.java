package net.eldiosantos.rpg.sheet.repository.hibernate.character.attribute;

import net.eldiosantos.rpg.sheet.model.character.attribute.CharacterMentalAttribute;
import net.eldiosantos.rpg.sheet.repository.character.attribute.CharacterMentalAttributeRepository;
import net.eldiosantos.rpg.sheet.repository.hibernate.base.BaseRepository;

/**
 * Created by Eldius on 12/05/2015.
 */
public class CharacterMentalAttributeHibernateRepositoryImpl extends BaseRepository<CharacterMentalAttribute, Long> implements CharacterMentalAttributeRepository {
}
