package net.eldiosantos.rpg.sheet.resource.skills;

import net.eldiosantos.rpg.sheet.model.skill.ExpertiseSkill;
import net.eldiosantos.rpg.sheet.repository.skill.ExpertiseSkillRepository;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by Eldius on 13/05/2015.
 */
@Path("skill/expertise")
public class ExpertiseSkillResource {

    @Inject
    private ExpertiseSkillRepository expertiseSkillRepository;

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<ExpertiseSkill> list() {
        return expertiseSkillRepository.listAll();
    }
}
