package net.eldiosantos.rpg.sheet.resource.skills;

import net.eldiosantos.rpg.sheet.model.skill.KnowledgeSkill;
import net.eldiosantos.rpg.sheet.repository.skill.KnowledgeSkillRepository;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by Eldius on 13/05/2015.
 */
@Path("skill/knowledge")
public class KnowledgeSkillResource {

    @Inject
    private KnowledgeSkillRepository knowledgeSkillRepository;

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<KnowledgeSkill> list() {
        return knowledgeSkillRepository.listAll();
    }
}
