package net.eldiosantos.rpg.sheet.resource.skills;

import net.eldiosantos.rpg.sheet.model.skill.TalentSkill;
import net.eldiosantos.rpg.sheet.repository.skill.TalentSkillRepository;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by Eldius on 13/05/2015.
 */
@Path("skill/talent")
public class TalentSkillResource {

    @Inject
    private TalentSkillRepository talentSkillRepository;

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<TalentSkill> list() {
        return talentSkillRepository.listAll();
    }
}
