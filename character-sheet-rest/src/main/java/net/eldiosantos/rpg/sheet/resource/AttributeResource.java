package net.eldiosantos.rpg.sheet.resource;

import net.eldiosantos.rpg.sheet.service.AttributeService;
import net.eldiosantos.rpg.sheet.vo.AttributeList;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by eldio.junior on 13/05/2015.
 */
@Path("attribute")
public class AttributeResource {

    @Inject
    private AttributeService attributeService;

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public AttributeList list() {
        return attributeService.list();
    }
}
