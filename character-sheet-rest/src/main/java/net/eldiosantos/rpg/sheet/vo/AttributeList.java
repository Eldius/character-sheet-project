package net.eldiosantos.rpg.sheet.vo;

import net.eldiosantos.rpg.sheet.model.attribute.FisicalAttribute;
import net.eldiosantos.rpg.sheet.model.attribute.MentalAttribute;
import net.eldiosantos.rpg.sheet.model.attribute.SocialAttribute;

import java.util.List;

/**
 * Created by eldio.junior on 13/05/2015.
 */
public class AttributeList {

    private List<FisicalAttribute>fisicalAttributes;
    private List<SocialAttribute>socialAttributes;
    private List<MentalAttribute>mentalAttributes;

    public List<FisicalAttribute> getFisicalAttributes() {
        return fisicalAttributes;
    }

    public AttributeList setFisicalAttributes(List<FisicalAttribute> fisicalAttributes) {
        this.fisicalAttributes = fisicalAttributes;
        return this;
    }

    public List<SocialAttribute> getSocialAttributes() {
        return socialAttributes;
    }

    public AttributeList setSocialAttributes(List<SocialAttribute> socialAttributes) {
        this.socialAttributes = socialAttributes;
        return this;
    }

    public List<MentalAttribute> getMentalAttributes() {
        return mentalAttributes;
    }

    public AttributeList setMentalAttributes(List<MentalAttribute> mentalAttributes) {
        this.mentalAttributes = mentalAttributes;
        return this;
    }
}
