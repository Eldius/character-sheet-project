package net.eldiosantos.rpg.sheet.resource.attributes;

import net.eldiosantos.rpg.sheet.model.attribute.SocialAttribute;
import net.eldiosantos.rpg.sheet.repository.attribute.SocialAttributeRepository;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by eldio.junior on 13/05/2015.
 */
@Path("attribute/social")
public class SocialAttributesResource {

    @Inject
    private SocialAttributeRepository socialAttributeRepository;

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<SocialAttribute> list() {
        return socialAttributeRepository.listAll();
    }

}
