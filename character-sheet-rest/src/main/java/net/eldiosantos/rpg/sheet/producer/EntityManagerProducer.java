package net.eldiosantos.rpg.sheet.producer;

import javax.enterprise.context.Dependent;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

@RequestScoped
public class EntityManagerProducer {

	@Inject
	private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("service");

	private EntityManager em;

	@PersistenceContext(unitName = "service")
	@Produces
	@Dependent
	public EntityManager getEntityManager() {
		if (em == null) {
			em = emf.createEntityManager();
		}

		return em;
	}

	public void dispose(@Disposes EntityManager entityManager) {
		entityManager.close();
	}
}
