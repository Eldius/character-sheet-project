package net.eldiosantos.rpg.sheet.config;

import org.glassfish.jersey.linking.DeclarativeLinkingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Eldius on 09/05/2015.
 */
public class RESTConfiguration extends ResourceConfig {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    public RESTConfiguration() {
        logger.debug("#######################################");
        logger.debug("Registering Declarative Hyperlinking...");
        register(DeclarativeLinkingFeature.class);
        logger.debug("Declarative Hyperlinking registered.");
        logger.debug("#######################################");
    }
}
