package net.eldiosantos.rpg.sheet.resource;

import net.eldiosantos.rpg.sheet.model.CharacterSheet;
import net.eldiosantos.rpg.sheet.repository.CharacterSheetRepository;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by Eldius on 12/05/2015.
 */
@Path("character")
public class CharacterResource {

    @Inject
    private CharacterSheetRepository characterSheetRepository;

    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<CharacterSheet>list() {
        return characterSheetRepository.listAll();
    }
}
