package net.eldiosantos.rpg.sheet.interceptor.persistence;

import javax.enterprise.inject.Any;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Transactional
@Interceptor
public class EntityTransactionInterceptor {

	private Logger logger = LoggerFactory.getLogger(getClass());

	private @Inject
	@Any
	EntityManager em;

	@AroundInvoke
	public Object aroundInvoke(InvocationContext context) throws Exception {

		Object result = null;

		logger.debug("starting aroundInvoke...");
		logger.debug("entityManager: " + em.toString());
		EntityTransaction transaction = em.getTransaction();
		logger.debug("Classe: " + context.getTarget().getClass().getSimpleName());
		boolean act = !transaction.isActive();
		logger.debug("act? " + act);
		if (act) {
			transaction.begin();
			logger.debug("transaction started...");
		}
		try {
			logger.debug("Proceeding process...");
			result = context.proceed();
			logger.debug("Process finished.");
			if (act) {
				logger.debug("Commiting transaction...");
				transaction.commit();
				logger.info("Transaction commited.");
			};
		} catch (Exception e) {
			if (act)
				transaction.rollback();
			logger.error("Error while managing transaction.", e);
			throw e;
		} finally {
			logger.info("Closing entity manager.");
			em.close();
		}
		return result;
	}
}
