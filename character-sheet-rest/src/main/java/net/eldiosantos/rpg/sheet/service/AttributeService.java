package net.eldiosantos.rpg.sheet.service;

import net.eldiosantos.rpg.sheet.repository.attribute.FisicalAttributeRepository;
import net.eldiosantos.rpg.sheet.repository.attribute.MentalAttributeRepository;
import net.eldiosantos.rpg.sheet.repository.attribute.SocialAttributeRepository;
import net.eldiosantos.rpg.sheet.vo.AttributeList;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by eldio.junior on 13/05/2015.
 */
public class AttributeService {

    @Inject
    private FisicalAttributeRepository fisicalAttributeRepository;
    @Inject
    private MentalAttributeRepository mentalAttributeRepository;
    @Inject
    private SocialAttributeRepository socialAttributeRepository;

    public AttributeList list() {
        return new AttributeList()
                .setFisicalAttributes(fisicalAttributeRepository.listAll())
                .setSocialAttributes(socialAttributeRepository.listAll())
                .setMentalAttributes(mentalAttributeRepository.listAll());
    }
}
