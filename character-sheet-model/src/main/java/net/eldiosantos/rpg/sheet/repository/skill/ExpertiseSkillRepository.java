package net.eldiosantos.rpg.sheet.repository.skill;

import net.eldiosantos.rpg.sheet.model.skill.ExpertiseSkill;
import net.eldiosantos.rpg.sheet.repository.interfaces.Repository;

public interface ExpertiseSkillRepository extends Repository<ExpertiseSkill, Long> {

}
