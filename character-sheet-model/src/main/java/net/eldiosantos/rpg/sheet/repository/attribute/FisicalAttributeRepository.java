package net.eldiosantos.rpg.sheet.repository.attribute;

import net.eldiosantos.rpg.sheet.model.attribute.FisicalAttribute;
import net.eldiosantos.rpg.sheet.repository.interfaces.Repository;

public interface FisicalAttributeRepository extends Repository<FisicalAttribute, Long> {

}
