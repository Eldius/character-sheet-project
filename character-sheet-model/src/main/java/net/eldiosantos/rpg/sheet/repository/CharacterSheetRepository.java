package net.eldiosantos.rpg.sheet.repository;

import net.eldiosantos.rpg.sheet.model.CharacterSheet;
import net.eldiosantos.rpg.sheet.repository.interfaces.Repository;

public interface CharacterSheetRepository extends Repository<CharacterSheet, Long> {

}
