package net.eldiosantos.rpg.sheet.repository.attribute;

import net.eldiosantos.rpg.sheet.model.attribute.SocialAttribute;
import net.eldiosantos.rpg.sheet.repository.interfaces.Repository;

public interface SocialAttributeRepository extends Repository<SocialAttribute, Long> {

}
