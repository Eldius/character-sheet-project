package net.eldiosantos.rpg.sheet.repository.skill;

import net.eldiosantos.rpg.sheet.model.skill.KnowledgeSkill;
import net.eldiosantos.rpg.sheet.repository.interfaces.Repository;

public interface KnowledgeSkillRepository extends Repository<KnowledgeSkill, Long> {

}
