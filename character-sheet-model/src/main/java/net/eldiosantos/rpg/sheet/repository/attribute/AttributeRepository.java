package net.eldiosantos.rpg.sheet.repository.attribute;

import net.eldiosantos.rpg.sheet.model.attribute.Attribute;
import net.eldiosantos.rpg.sheet.repository.interfaces.Repository;

public interface AttributeRepository extends Repository<Attribute, Long> {

}
