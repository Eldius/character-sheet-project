package net.eldiosantos.rpg.sheet.repository.skill;

import net.eldiosantos.rpg.sheet.model.skill.TalentSkill;
import net.eldiosantos.rpg.sheet.repository.interfaces.Repository;

public interface TalentSkillRepository extends Repository<TalentSkill, Long> {

}
