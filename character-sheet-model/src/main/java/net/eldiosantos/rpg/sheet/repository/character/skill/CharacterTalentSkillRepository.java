package net.eldiosantos.rpg.sheet.repository.character.skill;

import net.eldiosantos.rpg.sheet.model.character.skill.CharacterTalentSkill;
import net.eldiosantos.rpg.sheet.repository.interfaces.Repository;

/**
 * Created by Eldius on 13/05/2015.
 */
public interface CharacterTalentSkillRepository extends Repository<CharacterTalentSkill, Long> {
}
