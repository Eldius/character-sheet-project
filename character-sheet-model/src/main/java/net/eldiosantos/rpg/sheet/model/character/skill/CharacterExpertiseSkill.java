package net.eldiosantos.rpg.sheet.model.character.skill;


import net.eldiosantos.rpg.sheet.model.skill.ExpertiseSkill;

import javax.persistence.*;

/**
 * Created by eldio.junior on 13/05/2015.
 */
@Entity
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
public class CharacterExpertiseSkill extends CharacterSkill<ExpertiseSkill> {

}
