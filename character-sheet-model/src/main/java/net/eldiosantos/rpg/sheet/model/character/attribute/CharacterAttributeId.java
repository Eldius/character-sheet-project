package net.eldiosantos.rpg.sheet.model.character.attribute;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * Created by Eldius on 12/05/2015.
 */
public class CharacterAttributeId implements Serializable {

    private Long characterId;
    private Long attributeId;

    public Long getCharacterId() {
        return characterId;
    }

    public void setCharacterId(Long characterId) {
        this.characterId = characterId;
    }

    public Long getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(Long attributeId) {
        this.attributeId = attributeId;
    }
}
