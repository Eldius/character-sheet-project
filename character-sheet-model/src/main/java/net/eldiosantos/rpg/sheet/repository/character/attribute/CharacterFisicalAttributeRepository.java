package net.eldiosantos.rpg.sheet.repository.character.attribute;

import net.eldiosantos.rpg.sheet.model.character.attribute.CharacterFisicalAttribute;
import net.eldiosantos.rpg.sheet.repository.interfaces.Repository;

public interface CharacterFisicalAttributeRepository extends Repository<CharacterFisicalAttribute, Long> {

}
