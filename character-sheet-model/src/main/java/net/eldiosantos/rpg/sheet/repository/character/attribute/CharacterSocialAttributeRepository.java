package net.eldiosantos.rpg.sheet.repository.character.attribute;

import net.eldiosantos.rpg.sheet.model.character.attribute.CharacterSocialAttribute;
import net.eldiosantos.rpg.sheet.repository.interfaces.Repository;

public interface CharacterSocialAttributeRepository extends Repository<CharacterSocialAttribute, Long> {

}
