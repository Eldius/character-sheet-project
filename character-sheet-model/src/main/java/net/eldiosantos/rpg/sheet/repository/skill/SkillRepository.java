package net.eldiosantos.rpg.sheet.repository.skill;

import net.eldiosantos.rpg.sheet.model.skill.Skill;
import net.eldiosantos.rpg.sheet.repository.interfaces.Repository;

public interface SkillRepository extends Repository<Skill, Long> {

}
