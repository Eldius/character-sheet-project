package net.eldiosantos.rpg.sheet.repository.character.skill;

import net.eldiosantos.rpg.sheet.model.character.skill.CharacterSkill;
import net.eldiosantos.rpg.sheet.repository.interfaces.Repository;

/**
 * Created by Eldius on 13/05/2015.
 */
public interface CharacterSkillRepository extends Repository<CharacterSkill, Long> {
}
