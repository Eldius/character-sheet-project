package net.eldiosantos.rpg.sheet.model.character.attribute;

import net.eldiosantos.rpg.sheet.model.CharacterSheet;
import net.eldiosantos.rpg.sheet.model.attribute.Attribute;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;

/**
 * Created by eldio.junior on 12/05/2015.
 */
@MappedSuperclass
@IdClass(CharacterAttributeId.class)
public abstract class CharacterAttribute<T extends Attribute> {

    @Id
    @Column(name = "characterSheet_id")
    private Long characterId;

    @Id
    @Column(name = "attribute_id")
    private Long attributeId;

    @ManyToOne
    @JoinColumn(name = "characterSheet_id", updatable = false, insertable = false)
    private CharacterSheet characterSheet;

    @ManyToOne
    @JoinColumn(name = "attribute_id", updatable = false, insertable = false)
    private T attribute;

    private Integer value;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public CharacterSheet getCharacterSheet() {
        return characterSheet;
    }

    public void setCharacterSheet(CharacterSheet characterSheet) {
        this.characterSheet = characterSheet;
    }

    public T getAttribute() {
        return attribute;
    }

    public void setAttribute(T attribute) {
        this.attribute = attribute;
    }

    public Long getCharacterId() {
        return characterId;
    }

    public void setCharacterId(Long characterId) {
        this.characterId = characterId;
    }

    public Long getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(Long attributeId) {
        this.attributeId = attributeId;
    }

    public Long getId() {
        return attribute.getId();
    }

    public void setId(Long id) {
        attributeId = id;
    }

    public String getDisplayName() {
        return attribute.getDisplayName();
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
