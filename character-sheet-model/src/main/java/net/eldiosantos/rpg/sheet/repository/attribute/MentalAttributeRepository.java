package net.eldiosantos.rpg.sheet.repository.attribute;

import net.eldiosantos.rpg.sheet.model.attribute.MentalAttribute;
import net.eldiosantos.rpg.sheet.repository.interfaces.Repository;

public interface MentalAttributeRepository extends Repository<MentalAttribute, Long> {

}
