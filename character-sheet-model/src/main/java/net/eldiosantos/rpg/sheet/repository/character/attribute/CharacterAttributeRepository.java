package net.eldiosantos.rpg.sheet.repository.character.attribute;

import net.eldiosantos.rpg.sheet.model.character.attribute.CharacterAttribute;
import net.eldiosantos.rpg.sheet.repository.interfaces.Repository;

public interface CharacterAttributeRepository extends Repository<CharacterAttribute, Long> {

}
