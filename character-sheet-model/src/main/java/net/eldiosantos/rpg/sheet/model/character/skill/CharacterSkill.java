package net.eldiosantos.rpg.sheet.model.character.skill;

import net.eldiosantos.rpg.sheet.model.CharacterSheet;
import net.eldiosantos.rpg.sheet.model.skill.Skill;

import javax.persistence.*;

/**
 * Created by eldio.junior on 13/05/2015.
 */
@MappedSuperclass
@IdClass(CharacterSkillId.class)
public abstract class CharacterSkill<T extends Skill> {

    @Id
    @Column(name = "characterSheet_id")
    private Long characterId;

    @Id
    @Column(name = "skill_id")
    private Long skillId;

    @ManyToOne
    @JoinColumn(name = "characterSheet_id", updatable = false, insertable = false)
    private CharacterSheet characterSheet;

    @ManyToOne
    @JoinColumn(name = "skill_id", updatable = false, insertable = false)
    private T skill;

    private Integer value;

    public Long getCharacterId() {
        return characterId;
    }

    public void setCharacterId(Long characterId) {
        this.characterId = characterId;
    }

    public Long getSkillId() {
        return skillId;
    }

    public void setSkillId(Long skillId) {
        this.skillId = skillId;
    }

    public CharacterSheet getCharacterSheet() {
        return characterSheet;
    }

    public void setCharacterSheet(CharacterSheet characterSheet) {
        this.characterSheet = characterSheet;
    }

    public T getSkill() {
        return skill;
    }

    public void setSkill(T skill) {
        this.skill = skill;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Long getId() {
        return this.getSkill().getId();
    }

    public String getDisplayName() {
        return skill.getDisplayName();
    }

    public void setId(final Long id) {
        this.skillId = id;
    }
}
