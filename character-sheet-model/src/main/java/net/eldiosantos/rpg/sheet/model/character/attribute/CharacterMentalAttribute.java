package net.eldiosantos.rpg.sheet.model.character.attribute;

import net.eldiosantos.rpg.sheet.model.attribute.MentalAttribute;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 * Created by eldio.junior on 12/05/2015.
 */
@Entity
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
public class CharacterMentalAttribute extends CharacterAttribute<MentalAttribute> {
}
