package net.eldiosantos.rpg.sheet.model.character.skill;

import net.eldiosantos.rpg.sheet.model.skill.KnowledgeSkill;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 * Created by eldio.junior on 13/05/2015.
 */
@Entity
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
public class CharacterKnowledgeSkill extends CharacterSkill<KnowledgeSkill> {

}
