package net.eldiosantos.rpg.sheet.repository.character.attribute;

import net.eldiosantos.rpg.sheet.model.character.attribute.CharacterMentalAttribute;
import net.eldiosantos.rpg.sheet.repository.interfaces.Repository;

public interface CharacterMentalAttributeRepository extends Repository<CharacterMentalAttribute, Long> {

}
