package net.eldiosantos.rpg.sheet.model.character.skill;

import java.io.Serializable;

/**
 * Created by eldio.junior on 13/05/2015.
 */
public class CharacterSkillId implements Serializable {

    private Long characterId;
    private Long skillId;

    public Long getCharacterId() {
        return characterId;
    }

    public void setCharacterId(Long characterId) {
        this.characterId = characterId;
    }

    public Long getSkillId() {
        return skillId;
    }

    public void setSkillId(Long skillId) {
        this.skillId = skillId;
    }
}
