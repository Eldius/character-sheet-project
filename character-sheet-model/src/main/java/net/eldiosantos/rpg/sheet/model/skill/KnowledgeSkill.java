package net.eldiosantos.rpg.sheet.model.skill;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 * Created by eldio.junior on 12/05/2015.
 */
@Entity
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
public class KnowledgeSkill extends Skill {
}
