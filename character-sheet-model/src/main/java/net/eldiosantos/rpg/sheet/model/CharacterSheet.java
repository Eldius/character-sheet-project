package net.eldiosantos.rpg.sheet.model;

import net.eldiosantos.rpg.sheet.model.character.attribute.CharacterFisicalAttribute;
import net.eldiosantos.rpg.sheet.model.character.attribute.CharacterMentalAttribute;
import net.eldiosantos.rpg.sheet.model.character.attribute.CharacterSocialAttribute;
import net.eldiosantos.rpg.sheet.model.character.skill.CharacterExpertiseSkill;
import net.eldiosantos.rpg.sheet.model.character.skill.CharacterKnowledgeSkill;
import net.eldiosantos.rpg.sheet.model.character.skill.CharacterTalentSkill;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.util.List;

/**
 * Created by eldio.junior on 12/05/2015.
 */
@Entity
public class CharacterSheet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // First column
    private String name;
    private String player;
    private String story;

    // Second column
    private String nature;
    private String behavior;
    private String clan;

    // Thirsd column
    private String generation;
    private String haven;
    private String reputation;

    // Attributes
    @OneToMany(mappedBy = "characterSheet")
    private List<CharacterFisicalAttribute> fisicalAttributes;

    @OneToMany(mappedBy = "characterSheet")
    private List<CharacterSocialAttribute> socialAttributes;

    @OneToMany(mappedBy = "characterSheet")
    private List<CharacterMentalAttribute> mentalAttributes;

    // Skills
    @OneToMany(mappedBy = "characterSheet")
    private List<CharacterExpertiseSkill>expertiseSkills;

    @OneToMany(mappedBy = "characterSheet")
    private List<CharacterTalentSkill>talentSkills;

    @OneToMany(mappedBy = "characterSheet")
    private List<CharacterKnowledgeSkill>knowledgeSkills;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getBehavior() {
        return behavior;
    }

    public void setBehavior(String behavior) {
        this.behavior = behavior;
    }

    public String getClan() {
        return clan;
    }

    public void setClan(String clan) {
        this.clan = clan;
    }

    public String getGeneration() {
        return generation;
    }

    public void setGeneration(String generation) {
        this.generation = generation;
    }

    public String getHaven() {
        return haven;
    }

    public void setHaven(String haven) {
        this.haven = haven;
    }

    public String getReputation() {
        return reputation;
    }

    public void setReputation(String reputation) {
        this.reputation = reputation;
    }

    public List<CharacterFisicalAttribute> getFisicalAttributes() {
        return fisicalAttributes;
    }

    public void setFisicalAttributes(List<CharacterFisicalAttribute> fisicalAttributes) {
        this.fisicalAttributes = fisicalAttributes;
    }

    public List<CharacterSocialAttribute> getSocialAttributes() {
        return socialAttributes;
    }

    public void setSocialAttributes(List<CharacterSocialAttribute> socialAttributes) {
        this.socialAttributes = socialAttributes;
    }

    public List<CharacterMentalAttribute> getMentalAttributes() {
        return mentalAttributes;
    }

    public void setMentalAttributes(List<CharacterMentalAttribute> mentalAttributes) {
        this.mentalAttributes = mentalAttributes;
    }

    public List<CharacterExpertiseSkill> getExpertiseSkills() {
        return expertiseSkills;
    }

    public void setExpertiseSkills(List<CharacterExpertiseSkill> expertiseSkills) {
        this.expertiseSkills = expertiseSkills;
    }

    public List<CharacterTalentSkill> getTalentSkills() {
        return talentSkills;
    }

    public void setTalentSkills(List<CharacterTalentSkill> talentSkills) {
        this.talentSkills = talentSkills;
    }

    public List<CharacterKnowledgeSkill> getKnowledgeSkills() {
        return knowledgeSkills;
    }

    public void setKnowledgeSkills(List<CharacterKnowledgeSkill> knowledgeSkills) {
        this.knowledgeSkills = knowledgeSkills;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
