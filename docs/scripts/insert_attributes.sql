use development;

-- ATTRIBUTES:
-- Fisical attributes
INSERT INTO FisicalAttribute (displayName) VALUES ('Percepção');
INSERT INTO FisicalAttribute (displayName) VALUES ('Inteligência');
INSERT INTO FisicalAttribute (displayName) VALUES ('Raciocínio');

-- Social attributes
INSERT INTO SocialAttribute (displayName) VALUES ('Carisma');
INSERT INTO SocialAttribute (displayName) VALUES ('Manipulação');
INSERT INTO SocialAttribute (displayName) VALUES ('Aparência');

-- Mental attributes
INSERT INTO MentalAttribute (displayName) VALUES ('Força');
INSERT INTO MentalAttribute (displayName) VALUES ('Destreza');
INSERT INTO MentalAttribute (displayName) VALUES ('Vigor');

