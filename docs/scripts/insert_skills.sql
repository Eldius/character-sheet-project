-- SKILLS
-- Talents
INSERT INTO TalentSkill (displayName) VALUES ('Prontidão');
INSERT INTO TalentSkill (displayName) VALUES ('Esportes');
INSERT INTO TalentSkill (displayName) VALUES ('Briga');
INSERT INTO TalentSkill (displayName) VALUES ('Esquiva');
INSERT INTO TalentSkill (displayName) VALUES ('Empatia');
INSERT INTO TalentSkill (displayName) VALUES ('Expressão');
INSERT INTO TalentSkill (displayName) VALUES ('Intimidação');
INSERT INTO TalentSkill (displayName) VALUES ('Liderança');
INSERT INTO TalentSkill (displayName) VALUES ('Manha');
INSERT INTO TalentSkill (displayName) VALUES ('Lábia');

-- Expertises
INSERT INTO ExpertiseSkill (displayName) VALUES ('Emp. c/ animais');
INSERT INTO ExpertiseSkill (displayName) VALUES ('Ofícios');
INSERT INTO ExpertiseSkill (displayName) VALUES ('Condução');
INSERT INTO ExpertiseSkill (displayName) VALUES ('Etiqueta');
INSERT INTO ExpertiseSkill (displayName) VALUES ('Armas de fogo');
INSERT INTO ExpertiseSkill (displayName) VALUES ('Armas brancas');
INSERT INTO ExpertiseSkill (displayName) VALUES ('Performance');
INSERT INTO ExpertiseSkill (displayName) VALUES ('Segurança');
INSERT INTO ExpertiseSkill (displayName) VALUES ('Furtividade');
INSERT INTO ExpertiseSkill (displayName) VALUES ('Sobrevivência');

-- Knowledges
INSERT INTO KnowledgeSkill (displayName) VALUES ('Acadêmicos');
INSERT INTO KnowledgeSkill (displayName) VALUES ('Computador');
INSERT INTO KnowledgeSkill (displayName) VALUES ('Finanças');
INSERT INTO KnowledgeSkill (displayName) VALUES ('Investigação');
INSERT INTO KnowledgeSkill (displayName) VALUES ('Direito');
INSERT INTO KnowledgeSkill (displayName) VALUES ('Linguística');
INSERT INTO KnowledgeSkill (displayName) VALUES ('Medicina');
INSERT INTO KnowledgeSkill (displayName) VALUES ('Ocultismo');
INSERT INTO KnowledgeSkill (displayName) VALUES ('Política');
INSERT INTO KnowledgeSkill (displayName) VALUES ('Ciências');
